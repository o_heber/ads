import * as React from 'react';
import * as data from "../../data/coverage.json";
import logo from '../../images/logo.png';

export const Coverage: React.FC = () => {
    
    const [coverage, setCoverage] = React.useState(data.coverage);
    
    // Imprimer l'objet dans la console pour vérifier le changement
    React.useEffect(() =>  console.log(coverage), [coverage]);

    const handleChange = (model: string, year: number) => {
        const modelYears: Array<number> = coverage[model].slice();
        const index: number = modelYears.indexOf(year);

        if (index !== -1) {
            modelYears.splice(index, 1);
        } else {
            modelYears.push(year);
        }

        setCoverage({...coverage, [model]: modelYears});
    }

    return (
        <table>
            <thead>
                <tr>
                    <th key='logo'><div className='logo'><img src={logo} alt="Acura" /></div></th>
                    {data.years.map((year:number)=><th className='header' key={year}><div>{year}</div></th>)}
                </tr>
            </thead>
            <tbody>           
                {data['vehicle-models'].map((model: string)=>{
                    return <tr key={model} className='model-info'>
                        <td className='model-name' key={'label' + model}>{model}</td>
                    {data.years.map((year: number)=>{
                        return (
                            <td key={model + '-' + year}>
                                <input 
                                    className='compatibility' 
                                    type='checkbox' 
                                    defaultChecked={coverage[model].indexOf(year) !== -1? true : false}
                                    onChange={() => handleChange(model, year)} 
                                />
                            </td>
                        );
                    })}
                    </tr>                
                })}
            </tbody>
        </table>
    );
}